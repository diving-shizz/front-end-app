async function submitlog() 
{

    let record = {
        name: document.forms[0].elements[0].value,
        surname: document.forms[0].elements[1].value
    };
    let response = await fetch("https://europe-west2-divingshizz.cloudfunctions.net/back-end-app-reilly123-write", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
        },
        body: JSON.stringify(record)
    });
    const responseText = await response.text();
    console.log(responseText);
}
async function getlog()
{
    let response = await fetch("https://europe-west2-divingshizz.cloudfunctions.net/back-end-app-reilly123-read", {
        method: 'GET',
    });
    const responseText = await response.text();
    console.log(responseText);
    document.getElementById("dives").innerHTML = responseText;
}